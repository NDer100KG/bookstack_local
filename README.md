# Bookstack_local

host bookstack local server using docker, and link with the local-hosted draw.io

## linuxserver/bookstack
Use `docker-compose.yml` from [here](https://github.com/linuxserver/docker-bookstack/pkgs/container/bookstack)


## Solidnerd/bookstack, not recommanded
1. Clone docker-bookstack
```
git clone https://github.com/solidnerd/docker-bookstack.git
```
* note: the image tag `latest` not working

2. Edit `docker-compose.yml`
    * Change docker-bookstack image tag
    * Change mysql image tag
    * Adding `DRAWIO=http://localhost:5678/?embed=1&proto=json&spin=1` in the environment so that the bookstack will link to our local-hosted draw.io
    * Adding draw.io into the docker network held by this docker-compose

See [docker-compose.yml](docker-compose.yml) for the complete file
